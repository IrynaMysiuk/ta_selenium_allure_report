package com.epam.lab;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.epam.lab.singleton.DriverContainer.getDriver;

public class ListenerTest implements ITestListener {
    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Logger.getLogger("").log(Level.WARNING, "The name of the test case failed is " + iTestResult.getName());
        addScreenshot();
    }


    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Logger.getLogger("").log(Level.SEVERE, "The name of the test case skipped is " + iTestResult.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }

    @Attachment(value = "logs", type = "text/plain", fileExtension = ".log")
    public static byte[] attachFile(String path) {
        File file = new File(path);
        try {
            return Files.readAllBytes(Paths.get(file.getPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    private static byte[] addScreenshot() {
        return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}
